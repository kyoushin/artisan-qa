<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'question' => $faker->realText(50),
        'answered_right' => 0,
    ];
});

$factory->define(App\RightAnswer::class, function (Faker $faker) {
    return [
        'answer' => $faker->realText(20),
        'question_id' => function () {
            return factory(App\Question::class)->create()->id;
        }
    ];
});

$factory->define(App\Answers::class, function (Faker $faker) {
    return [
        'answer' => $faker->realText(20),
        'is_correct' => $faker->numberBetween(0, 1)
    ];
});