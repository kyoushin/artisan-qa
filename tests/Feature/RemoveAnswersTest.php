<?php

namespace Tests\Feature;

use App\Answers;
use App\Console\Handlers\QAndAHandler;
use App\Repositories\AnswersRepository;
use App\Repositories\QuestionsRepository;
use App\RightAnswer;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Mockery as m;

/**
 * Class RemoveAnswersTest
 * @package Tests\Feature
 */
class RemoveAnswersTest extends TestCase
{
    /**
     * @test
     */
    public function testRemoveAnswersCommand()
    {
        factory(RightAnswer::class, 5)->create();

        for ($x = 1; $x <= 5; $x++) {
            factory(Answers::class)->create([
                'question_id' => $x
            ]);
        }

        //before removal
        $this->assertEquals(Answers::count(), 5);

        Artisan::call('qanda:reset', ['--no-interaction' => true, 'confirm' => 'yes']);

        $this->assertEquals(Answers::count(), 0 );

        $this->assertContains('Successfully!', Artisan::output());
    }

    /**
     * @test
     */
    public function testRemoveAnswersCommandWorkflow()
    {
        $command = m::mock('App\Console\Commands\RemoveAnswers[confirm]', [new QAndAHandler(
            new QuestionsRepository(), new AnswersRepository()
        )]);

        factory(RightAnswer::class)->create();

        $command->shouldReceive('confirm')
            ->once()
            ->with('Are you sure to clean up the entire progress? The questions added will be kept.');

        $this->app['Illuminate\Contracts\Console\Kernel']->registerCommand($command);

        Artisan::call('qanda:reset', ['--no-interaction' => true]);
    }

    /**
     * @test
     */
    public function testRemoveAnswersCommandNoQuestions()
    {
        Artisan::call('qanda:reset', ['--no-interaction' => true]);

        $this->assertContains('Nothing to clean up.', Artisan::output());
    }
}
