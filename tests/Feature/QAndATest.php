<?php

namespace Tests\Feature;

use App\Answers;
use App\Console\Handlers\QAndAHandler;
use App\Console\Utils;
use App\Question;
use App\Repositories\AnswersRepository;
use App\Repositories\QuestionsRepository;
use App\RightAnswer;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery as m;

/**
 * Class QAndATest
 * @package Tests\Feature
 */
class QAndATest extends TestCase
{
    /**
     * Mocked command
     */
    protected $command;

    /**
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        /**
         * Partially mocking QAndA command
         */
        $this->command = m::mock(
            'App\Console\Commands\QAndA[choice, error]',
            [new QAndAHandler(
                new QuestionsRepository(), new AnswersRepository()
            ), new Utils()]
        );

        /**
         * Registering the mocked command
         */
        $this->app['Illuminate\Contracts\Console\Kernel']->registerCommand($this->command);
    }

    /**
     * @test
     */
    public function testWelcomeScreen()
    {
        $this->command->shouldReceive('choice')
            ->once()
            ->with('Which action would you like to call?', [
                'e' => 'Exit application',
                0   => 'Return back',
                1   => 'Add a question',
                2   => 'Practice',
                3   => 'View progress'
            ]);

        Artisan::call('qanda:interactive', ['--no-interaction' => true]);

        $this->assertContains('Welcome to the basic questions-answers educational app!', Artisan::output());
    }

    /**
     * @test
     */
    public function testEmptyProgress()
    {
        Artisan::call('qanda:interactive', ['--no-interaction' => true, 'input' => 3]);

        $this->assertContains('No progress so far', Artisan::output());
    }

    /**
     * @test
     */
    public function testHasProgress()
    {
        factory(RightAnswer::class, 5)->create();

        $answers = [];

        for ($x = 1; $x <= 5; $x++) {
            $answers[] = factory(Answers::class)->create([
                'question_id' => $x,
                'is_correct' => 0
            ]);
        }

        Artisan::call('qanda:interactive', ['--no-interaction' => true, 'input' => 3]);

        $output = Artisan::output();

        $this->assertContains('Total number of questions: 5', $output);
        $this->assertContains('Total number of answers: 5', $output);
        $this->assertContains('Correctly answered questions: 0', $output);

        foreach ($answers as $answer)
            $this->assertContains($answer->getText(), $output);
    }

    /**
     * @test
     */
    public function testHasPositiveProgress()
    {
        //Changing records as `answered` in order to check if the statistics will show correct numbers

        $originalAnswers = factory(RightAnswer::class, 5)->create();

        foreach ($originalAnswers as $originalAnswer)
            $originalAnswer->getQuestion()->setAsAnswered();

        $answers = [];

        for ($x = 1; $x <= 5; $x++) {
            $answers[] = factory(Answers::class)->create([
                'question_id' => $x,
                'is_correct' => 1
            ]);
        }

        Artisan::call('qanda:interactive', ['--no-interaction' => true, 'input' => 3]);

        $output = Artisan::output();

        $this->assertContains('Correctly answered questions: 5', $output);
    }

    /**
     * @test
     */
    public function testAddEmptyQuestion()
    {
        $this->command->shouldReceive('error')
            ->once()
            ->with('Question must have a text.');

        Artisan::call('qanda:interactive', ['--no-interaction' => true, 'input' => 1]);
    }

    /**
     * @test
     */
    public function testAddEmptyAnswerQuestion()
    {
        $this->command->shouldReceive('error')
            ->once()
            ->with('Answer is empty.');

        Artisan::call('qanda:interactive', [
            '--no-interaction' => true,
            'input' => 1,
            'firstParam' => 'A random question']
        );
    }

    /**
     * @test
     */
    public function testAddQuestion()
    {
        $this->command->shouldReceive('comment')
            ->atLeast()
            ->with('Question added successfully!');

        Artisan::call('qanda:interactive', [
                '--no-interaction' => true,
                'input' => 1,
                'firstParam' => 'A random question',
                'secondParam' => 'secretAnswer']
        );

        $this->assertTrue(
            Question::all()
                ->first()
                ->get()
                ->first()
                ->getText() == 'A random question'
        );

        $this->assertTrue(
            Question::all()
                ->first()
                ->get()
                ->first()
                ->getRightAnswer()
                ->getResults()
                ->getText() == 'secretAnswer'
        );
    }

    /**
     * @test
     */
    public function testAddExistingQuestion()
    {
        $answer = factory(RightAnswer::class)->create();
        $question = $answer->first()->getQuestion();

        $this->command->shouldReceive('error')
            ->once()
            ->with('This question is already in the database. Please add a different one.');

        Artisan::call('qanda:interactive', [
                '--no-interaction' => true,
                'input' => 1,
                'firstParam' => $question->getText(),
                'secondParam' => $answer->getText()]
        );
    }

    /**
     * @test
     */
    public function testPracticeNoQuestions()
    {
        $this->command->shouldReceive('error')
            ->once()
            ->with('There are no questions to practice.');

        Artisan::call('qanda:interactive', [
                '--no-interaction' => true,
                'input' => 2]
        );
    }

    /**
     * @test
     */
    public function testPracticeAllQuestionsAnswered()
    {
        $answer = factory(RightAnswer::class)->create();
        $answer->getQuestion()->setAsAnswered();

        factory(Answers::class)->create([
            'question_id' => $answer->getQuestion()->id,
            'is_correct' => 1
        ]);

        $this->command = m::mock(
            'App\Console\Commands\QAndA[comment]',
            [new QAndAHandler(
                new QuestionsRepository(), new AnswersRepository()
            ), new Utils()]
        );

        $this->command->shouldReceive('comment')
            ->twice();

        $this->app['Illuminate\Contracts\Console\Kernel']->registerCommand($this->command);

        Artisan::call('qanda:interactive', [
                '--no-interaction' => true,
                'input' => 2]
        );
    }
}
