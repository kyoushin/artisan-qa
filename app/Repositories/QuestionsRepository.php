<?php
namespace App\Repositories;

use App\Question;

/**
 * Class QuestionsRepository
 * @package App\Repositories
 */
class QuestionsRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $questions;

    /**
     * QuestionsRepository constructor.
     */
    public function __construct()
    {
        $this->questions = Question::query();
    }

    /**
     * @return $this
     */
    public function filterByCorrectAnswer()
    {
        $this->questions->where('answered_right', 1);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByUnanswered()
    {
        $this->questions->where('answered_right', 0);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByPreviousQuestions()
    {
        $this->filterByUnanswered();

        $this->questions->whereHas('userAnswers', function ($query) {
            $query->where('is_correct', 0);
        })->latest();

        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function filterByQuestionText(string $text)
    {
        $this->questions->where(
            'question', mb_strtolower($text, 'UTF-8')
        );

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function get()
    {
        return $this->questions;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->__construct();

        return $this;
    }
}