<?php
namespace App\Repositories;

use App\Answers;

/**
 * Class AnswersRepository
 * @package App\Repositories
 */
class AnswersRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $answers;

    /**
     * QuestionsRepository constructor.
     */
    public function __construct()
    {
        $this->answers = Answers::with('question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function get()
    {
        return $this->answers;
    }
}