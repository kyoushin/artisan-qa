<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseAnswer
 * @package App
 */
abstract class BaseAnswer extends Model
{
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->answer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function question()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }

    /**
     * @return \App\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->created_at;
    }
}