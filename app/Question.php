<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package App
 */
class Question extends Model
{
    /**
     * @var string
     */
    protected $table = 'questions';

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->question;
    }

    /**
     * @return bool
     */
    public function isAnsweredRight() : bool
    {
        return $this->answered_right;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getRightAnswer()
    {
        return $this->hasOne(RightAnswer::class, 'question_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAnswers()
    {
        return $this->hasMany(Answers::class, 'question_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getUserAnswers()
    {
        return $this->userAnswers;
    }

    /**
     * @return void
     */
    public function setAsAnswered()
    {
        $this->answered_right = true;

        $this->save();
    }
}
