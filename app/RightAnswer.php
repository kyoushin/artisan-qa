<?php
namespace App;

/**
 * Class RightAnswer
 * @package App
 */
class RightAnswer extends BaseAnswer
{
    /**
     * @var string
     */
    protected $table = 'answers';
}
