<?php
namespace App\Console\Traits;

/**
 * Trait Steps
 * @package App\Console\Traits
 */
trait Steps
{
    /**
     * @var array
     */
    protected $steps = [];

    /**
     * @var int
     */
    protected $position;

    /**
     * @var bool
     */
    protected $backwardsTraversing = false;

    /**
     * @var bool
     */
    protected $allowMoveCursorOnBackwards = false;

    /**
     * @param $method
     * @return void
     */
    public function registerStep($method)
    {
        if (!$this->backwardsTraversing)
            $this->steps[] = [
                'function' => $method,
            ];

        /**
         * Checking if we're right after oneStepBack() and preventing to increment cursor incorrectly
         */
        if ($this->position === null)
            $this->position = 0;
        else {
            if (!$this->backwardsTraversing)
                $this->position += 1;
            elseif ($this->allowMoveCursorOnBackwards && $this->backwardsTraversing)
                $this->allowMoveCursorOnBackwards = false;
            else $this->position += 1;
        }
    }

    /**
     * @return void
     */
    public function oneStepBack()
    {
        $this->backwardsTraversing = true;

        /**
         * Jumping one step back, moving cursor backwards
         */
        $this->position -= 1;
        $step = $this->steps[$this->position];

        $this->allowMoveCursorOnBackwards = true;

        call_user_func(__CLASS__ . '::' . $step['function']);
    }

    /**
     * @return void
     */
    public function goToFirstStep()
    {
        call_user_func(__CLASS__ . '::' . array_first($this->steps)['function']);
    }

    /**
     * @return void
     */
    public function resetPreviousSteps()
    {
        $this->backwardsTraversing = false;

        $this->steps = [$this->steps[0]];

        $this->position = 0;
    }
}