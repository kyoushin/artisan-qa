<?php
namespace App\Console;

use Illuminate\Support\Collection;

/**
 * Class Utils
 * @package App\Console
 */
class Utils
{
    /**
     * @param Collection $collection
     * @param string $key1
     * @param string $key2
     * @return array
     */
    public function extractPairs(Collection $collection, string $key1, string $key2) : array
    {
        return $collection->mapWithKeys(function ($item) use ($key1, $key2) {
            return [$item[$key1] => $item[$key2]];
        })->toArray();
    }

    /**
     * @param Collection $collection
     * @param int $inputId
     * @param string $key
     * @param string $primaryKey
     * @return string
     */
    public function extractKeyValue(Collection $collection, int $inputId, string $key, $primaryKey = 'input') : string
    {
        return $collection->where($primaryKey, $inputId)->first()[$key];
    }

    /**
     * @param Collection $untouchedCollection
     * @param string $secondaryKey
     * @return array
     */
    public function keysAsPrimaryKeysArray(Collection $untouchedCollection, $secondaryKey = 'question') : array
    {
        return array_combine(
            $untouchedCollection->pluck('id')->toArray(),
            $untouchedCollection->pluck($secondaryKey)->toArray()
        );
    }
}