<?php

namespace App\Console\Commands;

use App\Console\Handlers\QAndAHandler;
use App\Console\Traits\Steps;
use App\Console\Utils;
use App\Question;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

/**
 * Class QAndA
 * @package App\Console\Commands
 */
class QAndA extends Command
{
    use Steps;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:interactive {input?} {firstParam?} {secondParam?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs an interactive command line based Q And A system.';

    /**
     * @var QAndAHandler
     */
    protected $handler;

    /**
     * @var Utils
     */
    protected $utils;

    /**
     * Standard user choices, available anywhere within ask() and choice() input functions
     *
     * @var array
     */
    protected $commonChoices = [
        'e' => 'Exit application',
        0   => 'Return back'
    ];

    /**
     * @var bool
     */
    protected $greetingShown = false;

    /**
     * Dynamic routing to specified functions
     *
     * @var array|Collection
     */
    protected $primaryActions = [
        [
            'input' => 1,
            'name' => 'Add a question',
            'functionName' => 'addQuestion'
        ],
        [
            'input' => 2,
            'name' => 'Practice',
            'functionName' => 'practice'
        ],
        [
            'input' => 3,
            'name' => 'View progress',
            'functionName' => 'progress'
        ]
    ];

    protected $bootMethod = 'handle';

    /**
     * Create a new command instance.
     *
     * @param QAndAHandler $handler
     * @param Utils $utils
     */
    public function __construct(QAndAHandler $handler, Utils $utils)
    {
        $this->handler = $handler;
        $this->utils = $utils;

        $this->primaryActions = collect($this->primaryActions);

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->showGreeting();

        $this->registerStep(__FUNCTION__);
        $this->resetPreviousSteps();

        if (is_null($input = $this->argument('input'))) {
            $input = $this->choice('Which action would you like to call?',
                array_merge(
                    $this->commonChoices,
                    $this->utils->extractPairs($this->primaryActions, 'input', 'name')
                )
            );
        }

        /**
         * These checks will allow unit tests to pass through this code block when no input parameters specified
         */
        if ($this->isNotTestingEnvironment() || ($this->isTestingEnvironment() && $input !== null)) {
            if ($input == '0') {
                $this->comment('Cannot return back. You\'re on the uppermost level.');
                $this->goToFirstStep();
            } elseif ($input == 'e') {
                $this->info('Goodbye!');
                exit(0);
            } else
                call_user_func(__CLASS__ . '::' . $this->utils->extractKeyValue(
                    $this->primaryActions,
                    $input,
                    'functionName'
                ));
        }
    }

    /**
     * Add question subcommand.
     *
     * @return void
     */
    public function addQuestion()
    {
        $this->registerStep(__FUNCTION__);
        $this->showBasicInputInfo();

        if (is_null($question = $this->argument('firstParam')))
            $question = $this->ask('Define a new question:');

        if ($question === null)
            return $this->error('Question must have a text.');

        if ($this->backOrExitRequested($question))
            return $this->handleBasicInput($question);

        if ($this->handler->isQuestionUnique($question))
            return $this->error('This question is already in the database. Please add a different one.');
        else {
            $this->registerStep(__FUNCTION__);
            $this->showBasicInputInfo();

            if (is_null($answer = $this->argument('secondParam')))
                $answer = $this->secret('Fill in the according answer: (contents hidden)');

            if ($answer === null)
                return $this->error('Answer is empty.');

            if ($this->backOrExitRequested($answer))
                return $this->handleBasicInput($answer);

            if ($this->handler->saveQuestion($question, $answer))
                $this->comment('Question added successfully!');
            else
                $this->error('There was an error while saving it to the database.');
        }
    }

    /**
     * Practice subcommand.
     *
     * @param bool $showProgress
     * @return void
     */
    public function practice($showProgress = false)
    {
        $this->registerStep(__FUNCTION__);

        if (Question::count() == 0) {
            $this->error('There are no questions to practice.');
            return;
        }

        if ($showProgress) {
            $this->comment('Correctly answered questions:');

            $answeredQuestions = $this->handler->getQuestions(true);

            $progressTable = $this->handler->prepareQuestionsTable($answeredQuestions);

            $this->table($progressTable['header'], $progressTable['questions']);

            $this->output->newLine(1);

            $this->comment('Your overall progress:');

            $this->output->progressStart(Question::count());
            $this->output->progressAdvance($answeredQuestions->count());

            $this->output->newLine(2);
        }

        if (!$this->handler->questionsAvailable()) {
            $this->comment('Congradz, you have answered correctly to all questions!');
            return;
        } else {
            $prevQuestions = $this->handler->getPreviouslyUnansweredQuestions();

            if ($prevQuestions['count'] > 0) {
                $choiceText = 'Please, choose from the previous questions first:';
                $_questions = $prevQuestions['questions'];
            } else {
                $choiceText = 'Select a question to practice:';
                $_questions = $this->handler->getQuestions(false);
            }

            $q = $this->_choiceQuestion($choiceText, $_questions);

            if ($this->backOrExitRequested($q, false))
                return $this->handleBasicInput($q, false);

            $question = $_questions->find($q);

            $this->_askQuestion($question);

            $this->resetPreviousSteps();
            $this->practice(true);
        }
    }

    /**
     * Checking whatever the answer is correct
     *
     * @param Question $question
     * @return void
     */
    private function _askQuestion(Question $question)
    {
        $this->registerStep(__FUNCTION__);
        $this->showBasicInputInfo();

        $answer = $this->ask('Your answer:');

        if ($this->backOrExitRequested($answer))
            return $this->handleBasicInput($answer);

        $result = $this->handler->checkAnswer($question, (string)$answer);

        if ($result)
            $this->output->success('Correct answer!');
        else
            $this->error('Wrong answer!');

        $this->handler->saveAnswer($question, $answer, $result);
    }

    /**
     * "Adding" two arrays in order to avoid reindexing
     *
     * @param string $text
     * @param Collection $questions
     * @return string
     */
    private function _choiceQuestion(string $text, Collection $questions)
    {
        return $this->choice($text,
            $this->commonChoices + $this->utils->keysAsPrimaryKeysArray($questions)
        );
    }

    /**
     * Showing the greeting
     *
     * @return void
     */
    private function showGreeting()
    {
        if (!$this->greetingShown)
            $this->comment('Welcome to the basic questions-answers educational app!');

        $this->greetingShown = true;
    }

    /**
     * Using brackets in order to avoid answer with either exit or back code match
     *
     * @return void
     */
    private function showBasicInputInfo()
    {
        $this->comment(' Note : type {e} to exit the application, or {0} to move one step back.');

        $this->line('');
    }

    /**
     * @param string $input
     * @param bool $withBrackets
     * @return void
     */
    private function handleBasicInput(string $input, $withBrackets = true)
    {
        $exitSymbol = $withBrackets ? '{e}' : 'e';
        $backSymbol = $withBrackets ? '{0}' : '0';

        if ($input == $exitSymbol) {
            $this->info('Goodbye!');

            /**
             * Exiting with status code 0
             */
            exit(0);
        } elseif ($input == $backSymbol) {
            $this->oneStepBack();
        }
    }

    /**
     * @param $input
     * @param bool $withBrackets
     * @return bool
     * @return void
     */
    private function backOrExitRequested($input, $withBrackets = true)
    {
        $exitSymbol = $withBrackets ? '{e}' : 'e';
        $backSymbol = $withBrackets ? '{0}' : '0';

        return $input == $exitSymbol || $input == $backSymbol;
    }

    /**
     * Progress subcommand. Shows all user's answers along with some useful information
     *
     * @return void
     */
    public function progress()
    {
        $insights = $this->handler->getProgress();

        if ($insights === false)
            $this->info('No progress so far.');
        else {
            $this->comment('Current progress');

            $this->line('');

            $this->info(sprintf('Total number of questions: %s', $insights['stats']['questions']));
            $this->info(sprintf('Total number of answers: %s', $insights['stats']['totalAnswers']));
            $this->info(sprintf('Average attempts per question: %s', $insights['stats']['avgAttemptsPerQuestion']));
            $this->info(sprintf('Correctly answered questions: %s', $insights['stats']['answered']));

            $this->line('');

            $this->info('Displaying all your previous answers:');

            $this->table($insights['answers']['header'], $insights['answers']['answers']);

            $this->line('');

            $this->output->progressStart(Question::count());
            $this->output->progressAdvance($insights['stats']['answered']);

            $this->output->newLine(2);
        }
    }

    /**
     * @return bool
     */
    protected function isTestingEnvironment()
    {
        return app('env') == 'testing';
    }

    /**
     * @return bool
     */
    protected function isNotTestingEnvironment()
    {
        return app('env') != 'testing';
    }
}
