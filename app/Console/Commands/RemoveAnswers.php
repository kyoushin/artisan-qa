<?php

namespace App\Console\Commands;

use App\Console\Handlers\QAndAHandler;
use App\Question;
use Illuminate\Console\Command;

/**
 * Class RemoveAnswers
 * @package App\Console\Commands
 */
class RemoveAnswers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:reset {confirm?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all user answers and clean up his progress.';

    /**
     * @var QAndAHandler
     */
    protected $handler;

    /**
     * RemoveAnswers constructor.
     *
     * @param QAndAHandler $handler
     */
    public function __construct(QAndAHandler $handler)
    {
        parent::__construct();

        $this->handler = $handler;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Question::count() > 0) {

            if (is_null($confirm = $this->argument('confirm'))) {
                $confirm = $this->confirm('Are you sure to clean up the entire progress? The questions added will be kept.');
            }

            if ($confirm) {
                $this->handler->cleanProgress();

                $this->output->success('Successfully!');
            }
        } else
            $this->comment('Nothing to clean up.');
    }
}
