<?php
namespace App\Console\Handlers;

use App\Answers;
use App\Question;
use App\Repositories\AnswersRepository;
use App\Repositories\QuestionsRepository;
use App\RightAnswer;
use Illuminate\Support\Collection;

/**
 * Class QAndAHandler
 * @package App\Console\Handlers
 */
class QAndAHandler
{
    /**
     * @var QuestionsRepository
     */
    protected $questionsRepo;

    /**
     * @var AnswersRepository
     */
    protected $answersRepo;

    /**
     * Injecting repositories from laravel service container
     *
     * @param QuestionsRepository $repository
     * @param AnswersRepository $answersRepository
     */
    public function __construct(QuestionsRepository $repository, AnswersRepository $answersRepository)
    {
        $this->questionsRepo = $repository;
        $this->answersRepo = $answersRepository;
    }

    /**
     * @param string $question
     * @return bool
     */
    public function isQuestionUnique(string $question) : bool
    {
        return $this->questionsRepo
            ->reset()
            ->filterByQuestionText($question)
            ->get()
            ->exists();
    }

    /**
     * @param string $question
     * @param string $answer
     * @return bool
     */
    public function saveQuestion(string $question, string $answer) : bool
    {
        $_question = new Question();
        $_question->question = $question;

        $_questionSaved = $_question->save();

        $_answer = new RightAnswer();
        $_answer->answer = $answer;
        $_answer->question_id = $_question->id;

        $_answerSaved = $_answer->save();

        return $_questionSaved && $_answerSaved;
    }

    /**
     * @param Question $question
     * @param $answer
     * @param bool $isCorrect
     */
    public function saveAnswer(Question $question, $answer, bool $isCorrect)
    {
        if ($isCorrect)
            $question->setAsAnswered();

        $newAnswer = new Answers();
        $newAnswer->answer = $answer;
        $newAnswer->question_id = $question->id;
        $newAnswer->is_correct = $isCorrect;

        $newAnswer->save();
    }

    /**
     * @return bool
     */
    public function questionsAvailable() : bool
    {
        $unanswered = $this->questionsRepo
            ->reset()
            ->filterByUnanswered()
            ->get()
            ->count();

        return $unanswered > 0 && Question::count() > 0;
    }

    /**
     * @return array
     */
    public function getPreviouslyUnansweredQuestions()
    {
        $unanswered = $this->questionsRepo
            ->reset()
            ->filterByPreviousQuestions()
            ->get();

        return [
            'count' => $unanswered->count(),
            'questions' => $unanswered->get()
        ];
    }

    /**
     * @param bool $answered
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getQuestions(bool $answered)
    {
        $questions = $this->questionsRepo
            ->reset();

        $answered ? $questions->filterByCorrectAnswer()
            : $questions->filterByUnanswered();

        return $questions->get()->get();
    }

    /**
     * @param Collection $questions
     * @return array
     */
    public function prepareQuestionsTable(Collection $questions)
    {
        $_questions = $questions->map(function ($question) {
            return [
                'id' => $question->id,
                'question' => $question->getText(),
                'date' => $question->created_at
            ];
        });

        return [
            'questions' => $_questions,
            'header' => ['Id', 'Question', 'Date']
        ];
    }

    /**
     * @param Question $question
     * @param string $answer
     * @return bool
     */
    public function checkAnswer(Question $question, string $answer)
    {
        return mb_strtolower(
            $question->getRightAnswer()
                ->getResults()
                ->getText(), 'UTF-8') == mb_strtolower($answer, 'UTF-8');
    }

    /**
     * Removes the user progress.
     *
     * @return void
     */
    public function cleanProgress()
    {
        Answers::truncate();

        Question::query()->update([
            'answered_right' => false
        ]);
    }

    /**
     * @return array|bool
     */
    public function getProgress()
    {
        $_questions = $this->questionsRepo
            ->reset()
            ->get()
            ->get();

        if ($_questions->count() == 0)
            return false;

        $_answers = Answers::all();

        $progress['stats']['questions'] = $_questions->count();
        $progress['stats']['totalAnswers'] = $_answers->count();

        $avgAttemptsPerQuestion = $_answers->count() / $_questions->count();

        $progress['stats']['avgAttemptsPerQuestion'] = $avgAttemptsPerQuestion;

        $answered = $this->questionsRepo->filterByCorrectAnswer()->get()->get()->count();

        $progress['stats']['answered'] =  $answered;

        $progress['answers'] = $this->formatAnswersTable();

        return $progress;
    }

    /**
     * @return array
     */
    private function formatAnswersTable()
    {
        $allAnswers = $this->answersRepo->get()->get();

        $allAnswers = $allAnswers->map(function ($answer) {
            return [
                'question' => $answer->getQuestion()->getText(),
                'answer' => $answer->getText(),
                'answered_right' => $answer->isAnsweredRight() ? 'Yes' : 'No',
                'date' => $answer->getDate()
            ];
        });

        $header = [
            'Question',
            'Answer',
            'Answered right?',
            'Date'
        ];

        return [
            'answers' => $allAnswers->toArray(),
            'header' => $header
        ];
    }
}