<?php
namespace App;

/**
 * Class Answers
 * @package App
 */
class Answers extends BaseAnswer
{
    /**
     * @var string
     */
    protected $table = 'practiced';

    /**
     * @return bool
     */
    public function isAnsweredRight() : bool
    {
        return $this->is_correct;
    }
}
